create table coach_board(
  id serial primary key unique auto_increment not null,
  title varchar(255) not null,
  content varchar(255) not null,
  coach_id int not null,
  create_date datetime not null,
  update_date datetime not null
);

create table coach(
  id SERIAL primary key unique auto_increment not null,
  name varchar(255) not null,
  gender varchar(10) not null,
  age int not null,
  introduction varchar(255),
  image varchar(255) not null,
  user_id int not null,
  create_date DATETIME not null
);

create table user(
  id serial primary key unique not null auto_increment not null,
  login_id varchar(255) unique not null,
  password varchar(255) not null
);

create table user_board(
  id serial primary key unique auto_increment not null,
  title varchar(255) not null,
  content varchar(255) not null,
  user_id int not null,
  create_date datetime not null,
  update_date datetime not null
);
