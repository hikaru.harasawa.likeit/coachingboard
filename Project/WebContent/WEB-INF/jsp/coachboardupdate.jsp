<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%
String errMsg = (String) request.getAttribute("errMsg");
%>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/boardnew.css">
  <title>編集ページ</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="CoachTopServlet">CoachingBoard</a></h1>
    <nav>
      <ul class="main-nav">
        <li><a href="LogoutServlet">ログアウト</a></li>
      </ul>
    </nav>
  </header>
  <h1>掲示板編集</h1>

  <% if(errMsg != null){ %>
  <p><%= errMsg %></p>
  <% } %>

  <form action="CoachBoardUpdateServlet" method="post">
    <div class="form-item">
    <input type="hidden" name="id" value="${board.id}">
      <label for="title"></label>
      <input type="text" name="title" required="required" value="${board.title}">
    </div>
    <div class="form-item">
      <label for="content"></label>
      <textarea name="content">${board.content}</textarea>
    </div>
    <div class="button-panel">
      <input type="submit" class="button" value="編集する">
    </div>
  </form>
</body>
</html>
