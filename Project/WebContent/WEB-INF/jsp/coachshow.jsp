<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/coachshow.css">
  <title>掲示板詳細</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="IndexServlet">CoachingBoard</a></h1>
    <nav>
      <ul class="main-nav">
      <c:if test="${coachInfo != null}">
        <li><a href="BoardCreateServlet">新規投稿</a></li>
      </c:if>
      <c:if test="${userInfo != null}">
      	<li><a href="LogoutServlet">ログアウト</a></li>
      </c:if>
      <c:if test="${userInfo == null}">
        <li><a href="LoginServlet">ログイン</a></li>
      </c:if>
      </ul>
    </nav>
  </header>
      <div class="coaching-contents wrapper">
        <aside>
        　<img src="image/${coach.image}" width="130px" height="130px">
          <p>${coach.name}</p>
          <p>${coach.gender}</p>
          <p>${coach.age}</p>
		  <c:if test="${coachInfo.id == coach.id} or ${userInfo.login_id == 'admin'}">
          	<a href="CoachUpdateServlet?id=${coach.id}">編集</a>
          </c:if>
        </aside>
        <article>
          <p>${coach.introduction}</p>
          <c:if test="${coachInfo.id == coach.id}">
          	<a href="CoachUpdateServlet?id=${coach.id}">編集</a>
          </c:if>
          <c:if test="${userInfo.login_id == 'admin'}">
          	<a href="CoachUpdateServlet?id=${coach.id}">編集</a>
          </c:if>
        </article>
      </div>
      <div class="board2">
      <c:forEach var="b" items="${boardList}">
         <div class="board-title">${b.title}</div>
         <a href="BoardShowServlet?id=${b.id}">詳細</a>
         <c:if test="${coachInfo.id == b.coach_id}">
         	<a href="BoardUpdateServlet?id=${b.id}">編集</a>
        	<a href="BoardDeleteCheckServlet?id=${b.id}">削除</a>
         </c:if>
         <c:if test="${userInfo.login_id == 'admin'}">
         	<a href="BoardUpdateServlet?id=${b.id}">編集</a>
        	<a href="BoardDeleteCheckServlet?id=${b.id}">削除</a>
         </c:if>
        </c:forEach>
      </div>

</body>
</html>
