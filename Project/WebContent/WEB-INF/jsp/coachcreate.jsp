<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%
String errMsg = (String) request.getAttribute("errMsg");
%>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/boardnew.css">
  <title>コーチ登録</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="IndexServlet">CoachingBoard</a></h1>
    <nav>
      <ul class="main-nav">
        <li><a href="LogoutServlet">ログアウト</a></li>
      </ul>
    </nav>
  </header>
  <h1>コーチ登録</h1>

  <% if(errMsg != null){ %>
  <p><%= errMsg %></p>
  <% } %>

  <form action="CoachCreateServlet" enctype="multipart/form-data" method="post">
    <div class="form-item">
      <label for="name">名前</label>
      <input type="text" name="name" required="required">
    </div>
    <div class="form-item">
      <label for="sex"></label>
      <p>男性<input type="radio" name="gender" value="男性"></p>
      <p>女性<input type="radio" name="gender" value="女性"></p>
    </div>
    <div class="form-item">
      <label for="age">年齢</label>
      <input type="number" name="age">
    </div>
    <div class="form-item">
      <label for="introduction">自己紹介</label>
      <textarea name="introduction"></textarea>
    </div>
    <label class="upload-img-btn">
      画像を選択する
      <input type="file" name="image" style="display:none">
    </label>
    <div class="button-panel">
      <input type="submit" class="button" value="登録する">
    </div>
  </form>
</body>
</html>
