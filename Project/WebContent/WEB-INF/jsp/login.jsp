<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%
String errMsg = (String) request.getAttribute("errMsg");
%>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/login.css">
  <title>ログインフォーム</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="IndexServlet">CoachingBoard</a></h1>
  </header>
  <div class="form-wrapper">
    <h1>Login</h1>

    <% if(errMsg != null){ %>
    <p style="color: red; text-align:center;"><%= errMsg %></p>
    <% } %>

    <form action="LoginServlet" method="post">
      <div class="form-item">
        <label for="loginId"></label>
        <input type="text" name="login_id" required="required" placeholder="Id">
      </div>
      <div class="form-item">
        <label for="password"></label>
        <input type="password" name="password" required="required" placeholder="Password">
      </div>
      <div class="button-panel">
        <input type="submit" class="button" value="Sign In">
      </div>
    </form>
    <div class="form-footer">
      <p><a href="SignupServlet">Create an account</a></p>
    </div>
  </div>
</body>
</html>
