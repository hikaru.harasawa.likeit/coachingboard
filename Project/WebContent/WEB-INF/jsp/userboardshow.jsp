<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/login.css">
  <title>掲示板詳細</title>
</head>
<body>
  <header class="page-header">
  <c:if test="${userInfo != null}">
  	<h1><a href="UserTopServlet">CoachingBoard</a></h1>
  </c:if>
  <c:if test="${userInfo == null}">
    <h1><a href="IndexServlet">CoachingBoard</a></h1>
  </c:if>
    <nav>
      <ul class="main-nav">
        <li><a href="UserBoardCreateServlet">新規投稿</a></li>
        <li><a href="LogoutServlet">ログアウト</a></li>
      </ul>
    </nav>
  </header>
  <div class="form-wrapper">
    <div class="form-item">
      <h2>${board.title}</h2>
    </div>
    <div class="form-item">
      <p>${board.content}</p>
    </div>
    <div class="form-item">
      投稿者ID：${board.login_id}
      投稿日時：${board.create_date}
    </div>
  </div>
</body>
</html>
