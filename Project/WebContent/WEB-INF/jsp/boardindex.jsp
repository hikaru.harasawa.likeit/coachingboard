<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="css/index.css">
  <link rel= "stylesheet" href="css/simplePagination.css">

  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.simplePagination.js"></script>
  <title>掲示板一覧</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="IndexServlet">CoachingBoard</a></h1>
    <nav>
      <ul class="main-nav">
      	<c:if test="${coachInfo != null}">
       	  <li><a href="BoardCreateServlet">新規投稿</a></li>
      	</c:if>
      	<c:if test="${userInfo != null}">
      	  <li><a href="LogoutServlet">ログアウト</a></li>
      	</c:if>
     	<c:if test="${userInfo == null}">
          <li><a href="LoginServlet">ログイン</a></li>
      </c:if>
      </ul>
    </nav>
  </header>
  <h2>掲示板一覧</h2>
  <div class="container">
    <div class="selection" id="page-1">
  	<c:forEach var="b" items="${boardList}" begin="0" end="5">
    <div class="board-body">
         <div class="board-title">${b.title}</div><br>
         <a class="detail" href="BoardShowServlet?id=${b.id}">詳細</a><br>
         <div class="coach">
       	 	<a class="coach_image" href="CoachShowServlet?id=${b.coach_id}"><img src="image/${b.image}" width="85px" height="85px"></a>
         	<a class="coach_name" href="CoachShowServlet?id=${b.coach_id}">${b.coach_name}</a>
         </div>
      </div>
     </c:forEach>
    </div>
    <div class="selection" id="page-2">
    <c:forEach var="b" items="${boardList}" begin="6" end="10">
      <div class="board-body">
         <div class="board-title">${b.title}</div><br>
         <a class="detail" href="BoardShowServlet?id=${b.id}">詳細</a><br>
         <div class="coach">
       	 	<a class="coach_image" href="CoachShowServlet?id=${b.coach_id}"><img src="image/${b.image}" width="85px" height="85px"></a>
         	<a class="coach_name" href="CoachShowServlet?id=${b.coach_id}">${b.coach_name}</a>
         </div>
      </div>
     </c:forEach>
     </div>
  </div>

  <div class="pagination-holder clearfix">
    <div id="light-pagination" class="pagination"></div>
  </div>

  <script type="text/javascript">
    $(function(){
      $(".pagination").pagination({
        items:2,
        displayedPages:2,
        prevText:"prev",
        nextText:"next",
        cssStyle:'compact-theme',
        onPageClick:function(currentPageNumber){
          showPage(currentPageNumber);
        }
      })
    });
    function showPage(currentPageNumber){
      var page = "#page-" + currentPageNumber;
      $('.selection').hide();
      $(page).show();
    }
  </script>
  </body>
  </html>
