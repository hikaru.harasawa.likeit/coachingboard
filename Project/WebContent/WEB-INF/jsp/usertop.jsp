<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/swiper.min.css">

  <script src="js/swiper.min.js"></script>
  <script src="js/script.js"></script>

  <title>トップ画面</title>
</head>
<body>
  <header class="page-header">
  <c:if test="${userInfo != null}">
  	<h1><a href="UserTopServlet">CoachingBoard</a></h1>
  </c:if>
  <c:if test="${userInfo == null}">
    <h1><a href="IndexServlet">CoachingBoard</a></h1>
  </c:if>
    <nav>
      <ul class="main-nav">
      <c:if test="${userInfo != null}">
      	<li><a href="UserBoardCreateServlet?id=${userInfo.id}">新規投稿</a></li>
      	<li><a href="UserShowServlet?id=${userInfo.id}">投稿した掲示板</a></li>
       	<li><a href="LogoutServlet">ログアウト</a></li>
      </c:if>
      <c:if test="${coachInfo != null}">
       	<li><a href="CoachTopServlet">コーチ専用画面へ</a></li>
      </c:if>
      </ul>
    </nav>
  </header>
  <!--コーチングとはSTART -->
  <div class="l-wrapper">
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <div class="swiper-slide"><a href="#description"></a><img src="https://life-and-mind.com/wp-content/uploads/2019/06/ec_coaching02_001.png" alt="Swiper01" width="1000" height="500"></div>
      <div class="swiper-slide"><img src="https://and-plus.net/wp/wp-content/uploads/2015/03/c62110167a9822765cb853e08969febb.jpg" alt="Swiper02" width="1000" height="500"></div>
    </div>
    <div class="swiper-button-prev swiper-button-white"></div>
    <div class="swiper-button-next swiper-button-white"></div>
    <div class="swiper-pagination"></div>

  </div>
  </div>
  <!--コーチングとはEND -->

	<h2>コーチングを受けたい方</h2>
    <div class="wait">
      <a>コーチングをやってみたいという方があなたを待っています！</a>
    </div>
    <div class="container">
  	<c:forEach var="b" items="${boardList}">
      <div class="board-body">
         <div class="board-title">${b.title}</div><br>
         <a class="detail" href="CoachBoardShowServlet?id=${b.id}">詳細</a><br>
         <div class="coach">
       	 	<a class="coach_image" href="CoachShowServlet?id=${b.coach_id}"><img src="image/${b.image}" width="85px" height="85px"></a>
         	<a class="coach_name" href="CoachShowServlet?id=${b.coach_id}">${b.coach_name}</a>
         </div>
      </div>
     </c:forEach>
     <a href="CoachBoardIndexServlet">もっとみる＞</a>

  </div>

  <h2>コーチから探す</h2>
  <a href="CoachCreateServlet" class="btn-flat-border">コーチ登録はこちら</a>
  <div class="container">
  <c:forEach var="c" items="${coachList}">
    <div class="coach-body">
    <div class="coach2">
        <a class="coach_name2" href="CoachShowServlet?id=${c.id}">${c.name}</a>
        <a class="coach_image2" href="CoachShowServlet?id=${c.id}"><img src="image/${c.image}" width="130px" height="130px"></a>
    </div>
    </div>
   </c:forEach>
   <a href="CoachIndexServlet">もっとみる＞</a>
  </div>

  <h2>コーチングとは</h2>
    <div class="footer">
      <img src="image/660594670-huge.jpg" width="500px" height="300px">
      <a id="description">
        まず、コーチングとティーチングは異なる手段です。一般にティーチングは、親・先生・管理職などの立場の人が、豊かな知識や経験に基づき子・生徒・部下などを目標達成へと導くための指導方法です。
        そのため、指示・命令型の答えを与えるコミュニケーションに陥る傾向があるようです。<br><br>
        一方、「答えはその人の中にある」というコーチングの原則に基づき、コーチングでは「答えを与える」のではなく「答えを創り出す」サポートをします。
        コーチングでは「答え」を「外から与えられた答えは情報」として「自分の内にある答えを納得感」として位置づけており、後者を重視しています。
        コーチングでは両者を結びつけることで「その人自身の答え」になるとともに「答えを創り出す」ための基本としています。<br><br>
        このようにコーチングでは「答えはその人の中にある」という原則のもと、相手が状況に応じて自ら考え、行動した実感から学ぶことを支援し相手が本来持っている力や可能性を最大限に発揮できるよう
        サポートするためのコミュニケーション技術なのです。
      </a>
    </div>
    <br>(<a href="https://www.coachfederation.jp/ca/coaching/">参考にしたページ</a>)
</body>
</html>
