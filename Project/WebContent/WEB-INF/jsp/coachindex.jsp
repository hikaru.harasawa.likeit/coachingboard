<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="css/index.css">
  <link rel= "stylesheet" href="css/simplePagination.css">

  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.simplePagination.js"></script>
  <title>掲示板一覧</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="IndexServlet">CoachingBoard</a></h1>
    <nav>
      <ul class="main-nav">
      	<c:if test="${coachInfo != null}">
       	  <li><a href="BoardCreateServlet">新規投稿</a></li>
      	</c:if>
      	<c:if test="${userInfo != null}">
      	  <li><a href="LogoutServlet">ログアウト</a></li>
      	</c:if>
     	<c:if test="${userInfo == null}">
          <li><a href="LoginServlet">ログイン</a></li>
      </c:if>
      </ul>
    </nav>
  </header>
  <h2>コーチ一覧</h2>
  <div class="container">
    <div class="selection" id="page-1">
  	<c:forEach var="c" items="${coachList}" begin="0" end="5">
      <div class="coach-body">
      <div class="coach2">
          <a class="coach_name2" href="CoachShowServlet?id=${c.id}">${c.name}</a>
          <a class="coach_image2" href="CoachShowServlet?id=${c.id}"><img src="image/${c.image}" width="130px" height="130px"></a>
    </div>
    </div>
     </c:forEach>
    </div>

  </div>

  <div class="pagination-holder clearfix">
    <div id="light-pagination" class="pagination"></div>
  </div>

  <script type="text/javascript">
    $(function(){
      $(".pagination").pagination({
        items:1,
        displayedPages:1,
        prevText:"prev",
        nextText:"next",
        cssStyle:'compact-theme',
        onPageClick:function(currentPageNumber){
          showPage(currentPageNumber);
        }
      })
    });
    function showPage(currentPageNumber){
      var page = "#page-" + currentPageNumber;
      $('.selection').hide();
      $(page).show();
    }
  </script>
  </body>
  </html>
