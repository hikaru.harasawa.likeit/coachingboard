<%@ page language="java" contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/login.css">
  <title>掲示板詳細</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="IndexServlet">CoachingBoard</a></h1>
    <nav>
      <ul class="main-nav">
        <li><a href="BoardCreateServlet">新規投稿</a></li>
        <li><a href="LogoutServlet">ログアウト</a></li>
      </ul>
    </nav>
  </header>
  <div class="form-wrapper">
    <div class="form-item">
      <h2>${board.title}</h2>
    </div>
    <div class="form-item">
      <p>${board.content}</p>
    </div>
    <div class="form-item">
      <a href="CoachShowServlet?id=${board.coach_id}">${board.coach_name}</a>
    </div>
  </div>
</body>
</html>
