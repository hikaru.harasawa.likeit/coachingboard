<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/swiper.min.css">

  <script src="js/swiper.min.js"></script>
  <script src="js/script.js"></script>

  <title>掲示板一覧</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="UserTopServlet">CoachingBoard</a></h1>
    <nav>
      <ul class="main-nav">
      <c:if test="${userInfo != null}">
      	<li><a href="UserBoardCreateServlet?id=${userInfo.id}">新規投稿（ユーザー用）</a></li>
       	<li><a href="LogoutServlet">ログアウト</a></li>
      </c:if>

      </ul>
    </nav>
  </header>

	<h2>投稿した掲示板一覧</h2>

    <div class="container">
  	<c:forEach var="b" items="${boardList}">
      <div class="board-body">
         <div class="board-title">${b.title}</div><br>
         投稿日時：${b.update_date}
         <a class="detail" href="UserBoardShowServlet?id=${b.id}">詳細</a>
         <a class="detail" href="UserBoardUpdateServlet?id=${b.id}">編集</a>
         <a class="detail" href="UserBoardDeleteCheckServlet?id=${b.id}">削除</a>

      </div>
     </c:forEach>

  </div>
