<%@ page language="java" contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/login.css">
  <title>掲示板削除</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="IndexServlet">CoachingBoard</a></h1>
    <nav>
      <ul class="main-nav">
      	<li><a href="BoardCreateServlet?id=${coachInfo.id}">新規投稿</a></li>
      	<li><a href="CoachShowServlet?id=${coachInfo.id}">マイページ</a></li>
      	<li><a href="LogoutServlet">ログアウト</a></li>
      </ul>
    </nav>
  </header>
  <div class="form-wrapper">
    <div class="form-item">
      <h2>${board.title}</h2>
      <p>${board.content}</p><br>
      <p>を本当に削除しますか？</p><br>
      <a href="BoardDeleteServlet?id=${board.id}">はい</a>
      <a href="IndexServlet">いいえ</a>
    </div>
  </div>
</body>
</html>
