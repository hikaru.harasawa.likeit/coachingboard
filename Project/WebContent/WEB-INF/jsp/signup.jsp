<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%
String errMsg = (String) request.getAttribute("errMsg");
%>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/login.css">
  <title>新規登録</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="IndexServlet">CoachingBoard</a></h1>
  </header>
  <div class="form-wrapper">
    <h1>Sign up</h1>

    <% if(errMsg != null){ %>
    <p style="color: red; text-align: center;"><%= errMsg %></p>
    <% } %>

    <form action="SignupServlet" method="post">
      <div class="form-item">
        <label for="loginId"></label>
        <input type="text" name="login_id" required="required" placeholder="Id">
      </div>
      <div class="form-item">
        <label for="password"></label>
        <input type="password" name="password" required="required" placeholder="Password">
      </div>
      <div class="form-item">
        <label for="password"></label>
        <input type="password" name="password2" required="required" placeholder="Password(Confirmation)">
      </div>
      <div class="button-panel">
        <input type="submit" class="button" value="Create an account">
      </div>
    </form>
    <div class="form-footer">
      <p><a href="LoginServlet">アカウントをお持ちの方はこちら</a></p>
    </div>
  </div>
</body>
</html>
