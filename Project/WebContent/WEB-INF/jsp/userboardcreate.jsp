<%@ page language="java" contentType="text/html; charset=UTF-8" %>

<%
String errMsg = (String) request.getAttribute("errMsg");
%>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/boardnew.css">
  <title>新規投稿</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="UserTopServlet">CoachingBoard</a></h1>
    <nav>
      <ul class="main-nav">
        <li><a href="CoachShowServlet?id=${userInfo.id}">マイページ</a></li>
        <li><a href="LogoutServlet">ログアウト</a></li>
      </ul>
    </nav>
  </header>
  <h1>新規投稿</h1>

  <% if(errMsg != null){ %>
  <p><%= errMsg %></p>
  <% } %>

  <form action="UserBoardCreateServlet" method="post">
    <div class="form-item">
      <label for="title"></label>
      <input type="text" name="title" required="required" placeholder="タイトル">
    </div>
    <div class="form-item">
      <label for="content"></label>
      <textarea name="content" placeholder="内容"></textarea>
    </div>

    <div class="button-panel">
      <input type="submit" class="button" value="投稿する">
    </div>
  </form>
</body>
</html>
