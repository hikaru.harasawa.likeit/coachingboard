<%@ page language="java" contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/login.css">
  <title>ログアウト</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="IndexServlet">CoachingBoard</a></h1>
  </header>
  <div class="form-wrapper">
    <h2>ログアウトしました</h2>

    <div class="form-footer">
      <p><a href="LoginServlet">ログインはこちら</a></p>
    </div>
  </div>
</body>
</html>
