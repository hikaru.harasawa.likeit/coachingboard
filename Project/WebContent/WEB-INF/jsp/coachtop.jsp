<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/swiper.min.css">

  <script src="js/swiper.min.js"></script>
  <script src="js/script.js"></script>

  <title>トップ画面</title>
</head>
<body>
  <header class="page-header">
    <h1><a href="CoachTopServlet">CoachingBoard</a></h1>
    <nav>
      <ul class="main-nav">
      <c:if test="${coachInfo != null}">
       	<li><a href="CoachBoardCreateServlet?id=${coachInfo.id}">新規投稿</a></li>
       	<li><a href="CoachShowServlet?id=${coachInfo.id}">マイページ</a></li>
       	<li><a href="LogoutServlet">ログアウト</a></li>
       	<li><a href="UserTopServlet">ユーザー専用画面へ</a></li>
      </c:if>
      </ul>
    </nav>
  </header>
  <!--コーチングとはSTART -->
  <div class="l-wrapper">
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <div class="swiper-slide"><a href="#description"></a><img src="https://life-and-mind.com/wp-content/uploads/2019/06/ec_coaching02_001.png" alt="Swiper01" width="1000" height="500"></div>
      <div class="swiper-slide"><img src="https://and-plus.net/wp/wp-content/uploads/2015/03/c62110167a9822765cb853e08969febb.jpg" alt="Swiper02" width="1000" height="500"></div>
    </div>
    <div class="swiper-button-prev swiper-button-white"></div>
    <div class="swiper-button-next swiper-button-white"></div>
    <div class="swiper-pagination"></div>

  </div>
  </div>
  <!--コーチングとはEND -->

  <h2>コーチングをやってみたい方</h2>
  <div class="wait">
    <a>コーチングを受けたいという方があなたを待っています！</a>
  </div>
  <div class="container">
  <c:forEach var="b" items="${boardList}">
    <div class="board-body">
      <div class="board-title">${b.title}</div><br>
      <a class="detail" href="UserBoardShowServlet?id=${b.id}">詳細</a>
      <div class="coach">
        <a class="coach_name">${b.login_id}</a>
      </div>
    </div>
    </c:forEach>
    <a href="UserBoardIndexServlet">もっとみる＞</a>
  </div>

  <h2>コーチングとは</h2>
    <div class="footer">
      <img src="image/660594670-huge.jpg" width="500px" height="300px">
      <a id="description">
        まず、コーチングとティーチングは異なる手段です。一般にティーチングは、親・先生・管理職などの立場の人が、豊かな知識や経験に基づき子・生徒・部下などを目標達成へと導くための指導方法です。
        そのため、指示・命令型の答えを与えるコミュニケーションに陥る傾向があるようです。<br><br>
        一方、「答えはその人の中にある」というコーチングの原則に基づき、コーチングでは「答えを与える」のではなく「答えを創り出す」サポートをします。
        コーチングでは「答え」を「外から与えられた答えは情報」として「自分の内にある答えを納得感」として位置づけており、後者を重視しています。
        コーチングでは両者を結びつけることで「その人自身の答え」になるとともに「答えを創り出す」ための基本としています。<br><br>
        このようにコーチングでは「答えはその人の中にある」という原則のもと、相手が状況に応じて自ら考え、行動した実感から学ぶことを支援し相手が本来持っている力や可能性を最大限に発揮できるよう
        サポートするためのコミュニケーション技術なのです。
      </a>
    </div>
    <br>(<a href="https://www.coachfederation.jp/ca/coaching/">参考にしたページ</a>)
</body>
</html>
