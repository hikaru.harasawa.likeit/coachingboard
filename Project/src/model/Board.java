package model;

import java.io.Serializable;

public class Board implements Serializable{
	private int id;
	private String login_id;
	private String title;
	private String content;
	private int coach_id;
	private int user_id;
	private String coach_name;
	private String image;
	private String create_date;
	private String update_date;

	public Board(int id, String title, String content, int coach_id, String coach_name, String create_date, String update_date) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.coach_id = coach_id;
		this.coach_name = coach_name;
		this.create_date = create_date;
		this.update_date = update_date;
	}

	public Board(int id, String title, String content, int coach_id, String coach_name, String image, String create_date, String update_date) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.coach_id = coach_id;
		this.coach_name = coach_name;
		this.image = image;
		this.create_date = create_date;
		this.update_date = update_date;
	}

	public Board(int id, String login_id, String title, String content, int user_id, String create_date, String update_date) {
		this.id = id;
		this.login_id = login_id;
		this.title = title;
		this.content = content;
		this.user_id = user_id;
		this.create_date = create_date;
		this.update_date = update_date;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getCoach_id() {
		return coach_id;
	}

	public void setCoach_id(int coach_id) {
		this.coach_id = coach_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getCoach_name() {
		return coach_name;
	}

	public void setCoach_name(String coach_name) {
		this.coach_name = coach_name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
}
