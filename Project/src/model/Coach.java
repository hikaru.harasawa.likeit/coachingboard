package model;

import java.io.Serializable;

public class Coach implements Serializable{
	private int id;
	private String name;
	private String gender;
	private int age;
	private String introduction;
	private String image;
	private String create_date;

	public Coach(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public Coach(int id, String name, String gender, int age, String introduction,  String image) {
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.introduction = introduction;
		this.image =image;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}

}
