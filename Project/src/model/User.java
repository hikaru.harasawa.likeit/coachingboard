package model;

import java.io.Serializable;

public class User implements Serializable{
	private int id;
	private String login_id;
	private String password;

	public User(int id, String login_id) {
		this.id = id;
		this.login_id = login_id;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
