package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Board;

public class UserBoardDao{

	private Connection con = null;
	private Statement stmt = null;
	private PreparedStatement ps = null;

	public List<Board> findLatest(){
		List<Board>boardList = new ArrayList<Board>();
		try {
			con = DBManager.getConnection();
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM user_board INNER JOIN user ON user_board.user_id = user.id ORDER BY update_date DESC LIMIT 3");

			while(rs.next()) {
				int board_id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String title = rs.getString("title");
				String content = rs.getString("content");
				int user_id = rs.getInt("user_id");
				String create_date = rs.getString("create_date");
				String update_date = rs.getString("update_date");
				Board board = new Board(board_id, login_id, title, content, user_id, create_date, update_date);
				boardList.add(board);
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return boardList;
	}

	public List<Board> findAll(){
		List<Board> boardList = new ArrayList<Board>();
		try {
			con = DBManager.getConnection();
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM user_board INNER JOIN user ON user_board.user_id = user.id ORDER BY update_date DESC");

			while(rs.next()) {
				int board_id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String title = rs.getString("title");
				String content = rs.getString("content");
				int user_id = rs.getInt("user_id");
				String create_date = rs.getString("create_date");
				String update_date = rs.getString("update_date");
				Board board = new Board(board_id, login_id, title, content, user_id,create_date, update_date);
				boardList.add(board);
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return boardList;
	}

	public void CreateUserBoard(String title, String content, int user_id) {
		try {
			con = DBManager.getConnection();
			ps = con.prepareStatement("INSERT INTO user_board(title,content,user_id,create_date,update_date) VALUES (?,?,?,now(),now())");
			ps.setString(1, title);
			ps.setString(2, content);
			ps.setInt(3, user_id);
			ps.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(con != null) {
				try {
					con.close();

				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public Board findUserBoardByBoardId(int board_id) {
		try {
			con = DBManager.getConnection();
			ps = con.prepareStatement("SELECT * FROM user_board INNER JOIN user ON user_board.user_id = user.id WHERE user.id=?");
			ps.setInt(1, board_id);
			ResultSet rs = ps.executeQuery();
			if(!rs.next()) {
				return null;
			}
			int id = rs.getInt("id");
			String login_id = rs.getString("login_id");
			String title = rs.getString("title");
			String content = rs.getString("content");
			int user_id = rs.getInt("user_id");
			String create_date = rs.getString("create_date");
			String update_date = rs.getString("update_date");

			return new Board(id,login_id,title,content,user_id,create_date,update_date);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void UpdateUserBoard(int board_id, String title, String content) {
		try {
			con = DBManager.getConnection();
			ps = con.prepareStatement("UPDATE user_board SET title=?, content=? WHERE id=?");
			ps.setString(1, title);
			ps.setString(2, content);
			ps.setInt(3, board_id);
			ps.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void DeleteUserBoard(int board_id) {
		try {
			con = DBManager.getConnection();
			ps = con.prepareStatement("DELETE FROM user_board WHERE id=?");
			ps.setInt(1, board_id);
			ps.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
