package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import com.mysql.jdbc.PreparedStatement;

import model.Board;
import model.Coach;
import model.User;

public class UserDao{

	private Connection con = null;
	private PreparedStatement ps = null;

	//暗号化
	public String encrypto(String password) throws Exception{
		String source = password;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";

		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);
		return result;
	}

	public User findByLoginInfo(String login_id, String password) {

		try {
			con = DBManager.getConnection();
			String p = null;
			try {
				p = encrypto(password);
			}catch(Exception e) {
				e.printStackTrace();
			}
			ps = (PreparedStatement) con.prepareStatement("SELECT * from user WHERE login_id=? and password=?");
			ps.setString(1, login_id);
			ps.setString(2, p);
			ResultSet rs = ps.executeQuery();

			if(!rs.next()) {
				return null;
			}
			int userIdDate = rs.getInt("id");
			String loginIdDate = rs.getString("login_id");
			return new User(userIdDate,loginIdDate);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<Board> findBoardByUserId(int userId){
		List<Board> boardList = new ArrayList<Board>();
		try {
			con = DBManager.getConnection();
			ps = (PreparedStatement) con.prepareStatement("SELECT * FROM user_board INNER JOIN user ON user_board.user_id = user.id WHERE user_board.user_id=?");
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				int board_id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String title = rs.getString("title");
				String content = rs.getString("content");
				int user_id = rs.getInt("user_id");
				String create_date = rs.getString("create_date");
				String update_date = rs.getString("update_date");
				Board board = new Board(board_id,login_id,title,content,user_id,create_date,update_date);
				boardList.add(board);
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return boardList;
	}

	public Coach findCoachInfoByUserInfo(String login_id, String password) {
		try {
			con = DBManager.getConnection();
			String p = null;
			try {
				p = encrypto(password);
			}catch(Exception e) {
				e.printStackTrace();
			}
			ps = (PreparedStatement) con.prepareStatement("SELECT coach.id,coach.name from coach INNER JOIN user ON coach.user_id=user.id WHERE user.login_id=? and user.password=?");
			ps.setString(1, login_id);
			ps.setString(2, p);
			ResultSet rs = ps.executeQuery();

			if(!rs.next()) {
				return null;
			}
			int coachIdDate = rs.getInt("id");
			String coachNameDate = rs.getString("name");
			return new Coach(coachIdDate,coachNameDate);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void Signup(String login_id,String password) {
		try {
			con = DBManager.getConnection();
			String p = null;
			try {
				p = encrypto(password);
			}catch(Exception e) {
				e.printStackTrace();
			}
			ps = (PreparedStatement) con.prepareStatement("INSERT INTO user(login_id,password) VALUES(?,?)");
			ps.setString(1, login_id);
			ps.setString(2, p);
			ps.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean loginId_check(String login_id) {
		try {
			con = DBManager.getConnection();
			ps = (PreparedStatement) con.prepareStatement("SELECT * FROM user WHERE login_id=?");
			ps.setString(1, login_id);
			ResultSet rs = ps.executeQuery();

			if(rs.next()) {
				return false;
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return (Boolean) null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
					return (Boolean)null;
				}
			}
		}
		return true;
	}
}
