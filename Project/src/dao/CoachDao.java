package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Board;
import model.Coach;

public class CoachDao{
	private Connection con = null;
	private Statement stmt = null;
	private PreparedStatement ps = null;

	public List<Coach> findLatest(){
		List<Coach> coachList = new ArrayList<Coach>();
		try {
			con = DBManager.getConnection();
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT*FROM coach ORDER BY id DESC LIMIT 3");

			while(rs.next()) {
				int coach_id = rs.getInt("id");
				String name = rs.getString("name");
				String gender = rs.getString("gender");
				int age = rs.getInt("age");
				String introduction = rs.getString("introduction");
				String image = rs.getString("image");
				Coach coach = new Coach(coach_id, name, gender, age, introduction, image);
				coachList.add(coach);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return coachList;
	}

	public List<Coach> findAll(){
		List<Coach> coachList = new ArrayList<Coach>();
		try {
			con = DBManager.getConnection();
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT*FROM coach ORDER BY create_date DESC");

			while(rs.next()) {
				int coach_id = rs.getInt("id");
				String name = rs.getString("name");
				String gender = rs.getString("gender");
				int age = rs.getInt("age");
				String introduction = rs.getString("introduction");
				String image = rs.getString("image");
				Coach coach = new Coach(coach_id, name, gender, age, introduction, image);
				coachList.add(coach);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return coachList;
	}

	public void CreateCoach(String name, String gender, int age, String introduction, String image, int user_id) {
		try {
			con = DBManager.getConnection();
			ps = con.prepareStatement("INSERT INTO coach(name,gender,age,introduction,image,create_date,user_id) VALUES (?,?,?,?,?,now(),?)");
			ps.setString(1, name);
			ps.setString(2, gender);
			ps.setInt(3, age);
			ps.setString(4, introduction);
			ps.setString(5, image);
			ps.setInt(6, user_id);
			ps.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(con != null) {
				try {
					con.close();

				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	//自己紹介無し
	public void CreateCoach2(String name, String gender, int age,String image, int user_id) {
		try {
			con = DBManager.getConnection();
			ps = con.prepareStatement("INSERT INTO coach(name,gender,age,image,create_date,user_id) VALUES (?,?,?,?,now(),?)");
			ps.setString(1, name);
			ps.setString(2, gender);
			ps.setInt(3, age);
			ps.setString(4, image);
			ps.setInt(5, user_id);
			ps.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(con != null) {
				try {
					con.close();

				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public Coach findCoachByCoachId(int coach_id) {
		try {
			con = DBManager.getConnection();
			ps = con.prepareStatement("SELECT*FROM coach WHERE id=?");
			ps.setInt(1, coach_id);
			ResultSet rs = ps.executeQuery();
			if(!rs.next()) {
				return null;
			}
			int id = rs.getInt("id");
			String name = rs.getString("name");
			String gender = rs.getString("gender");
			int age = rs.getInt("age");
			String introduction = rs.getString("introduction");
			String image = rs.getString("image");
			return new Coach(id,name,gender,age,introduction,image);
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<Board> findBoardByCoachId(int coachId) {
		List<Board> boardList = new ArrayList<Board>();
		try {
			con = DBManager.getConnection();
			ps = con.prepareStatement("SELECT*FROM coach_board INNER JOIN coach ON coach_board.coach_id = coach.id WHERE coach_board.coach_id=?");
			ps.setInt(1, coachId);
			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				int board_id = rs.getInt("id");
				String title = rs.getString("title");
				String content = rs.getString("content");
				int coach_id = rs.getInt("coach_id");
				String coach_name = rs.getString("name");
				String create_date = rs.getString("create_date");
				String update_date = rs.getString("update_date");
				Board board = new Board(board_id,title,content,coach_id,coach_name,create_date,update_date);
				boardList.add(board);
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return boardList;
	}

	public void UpdateCoach(String name, int age, String introduction, String image, int coach_id) {
		try {
			con = DBManager.getConnection();
			ps = con.prepareStatement("UPDATE coach SET name=?, age=?, introduction=?, image=? WHERE id=?");
			ps.setString(1, name);
			ps.setInt(2, age);
			ps.setString(3, introduction);
			ps.setString(4, image);
			ps.setInt(5, coach_id);
			ps.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
