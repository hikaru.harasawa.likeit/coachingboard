package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserBoardDao;
import model.User;


@WebServlet("/UserBoardCreateServlet")
public class UserBoardCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserBoardCreateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		User userInfo = (User)session.getAttribute("userInfo");
		if(userInfo == null) {
			response.sendRedirect("IndexServlet");
			return;
		}
		session.setAttribute("userInfo", userInfo);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userboardcreate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession(false);
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		User userInfo = (User)session.getAttribute("userInfo");
		int user_id = userInfo.getId();

		if(title.equals("") || content.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userboardcreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		UserBoardDao boarddao = new UserBoardDao();
		boarddao.CreateUserBoard(title,content,user_id);
		response.sendRedirect("UserTopServlet");
	}

}
