package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import dao.CoachDao;
import model.User;

@WebServlet("/CoachCreateServlet")
@MultipartConfig(location="/Applications/Eclipse_4.7.3.app/Contents/workspace/CoachingBoard/WebContent/image", maxFileSize=1048576)
public class CoachCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public CoachCreateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		User userInfo = (User)session.getAttribute("userInfo");
		if(userInfo == null) {
			response.sendRedirect("IndexServlet");
			return;
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coachcreate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession(false);
		User userInfo = (User)session.getAttribute("userInfo");

		String name = request.getParameter("name");
		String gender = request.getParameter("gender");
		int age = Integer.parseInt(request.getParameter("age"));
		String introduction = request.getParameter("introduction");

		Part imageFile = request.getPart("image");
		String image = getFileName(imageFile);
		imageFile.write(image);

		int user_id = userInfo.getId();

		if(name.equals("") || gender.equals("") || age == 0 || image.equals("")) {
			request.setAttribute("errMsg", "正しく入力されていません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/coachcreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		CoachDao coachdao = new CoachDao();
		if(introduction.equals("")) {
			coachdao.CreateCoach2(name,gender,age,image,user_id);
		}
		coachdao.CreateCoach(name,gender,age,introduction,image,user_id);

		response.sendRedirect("IndexServlet");
	}

	private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }

}
