package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserBoardDao;
import model.Board;
import model.User;

@WebServlet("/UserBoardUpdateServlet")
public class UserBoardUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserBoardUpdateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		User userInfo = (User)session.getAttribute("userInfo");
		if(userInfo == null) {
			response.sendRedirect("IndexServlet");
			return;
		}
		int id = Integer.parseInt(request.getParameter("id"));
		UserBoardDao boarddao = new UserBoardDao();
		Board board = boarddao.findUserBoardByBoardId(id);
		request.setAttribute("board", board);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userboardupdate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		int board_id = Integer.parseInt(request.getParameter("id"));
		String title = request.getParameter("title");
		String content = request.getParameter("content");

		if(title.equals("") || content.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coachboardupdate.jsp");
			dispatcher.forward(request, response);
		}

		UserBoardDao boarddao = new UserBoardDao();
		boarddao.UpdateUserBoard(board_id, title, content);
		response.sendRedirect("UserTopServlet");
	}

}
