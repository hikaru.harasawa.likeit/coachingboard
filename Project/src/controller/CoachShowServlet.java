package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CoachDao;
import model.Board;
import model.Coach;


@WebServlet("/CoachShowServlet")
public class CoachShowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public CoachShowServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int coach_id = Integer.parseInt(request.getParameter("id"));
		CoachDao coachdao = new CoachDao();
		Coach coach = coachdao.findCoachByCoachId(coach_id);
		List<Board> boardList = coachdao.findBoardByCoachId(coach_id);
		request.setAttribute("coach", coach);
		request.setAttribute("boardList", boardList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coachshow.jsp");
		dispatcher.forward(request, response);
	}

}
