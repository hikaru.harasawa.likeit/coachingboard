package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserBoardDao;
import model.Board;

@WebServlet("/UserBoardShowServlet")
public class UserBoardShowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserBoardShowServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int board_id = Integer.parseInt(request.getParameter("id"));
		UserBoardDao boarddao = new UserBoardDao();
		Board board = boarddao.findUserBoardByBoardId(board_id);
		request.setAttribute("board", board);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userboardshow.jsp");
		dispatcher.forward(request, response);
	}
}
