package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CoachBoardDao;
import model.Coach;

@WebServlet("/CoachBoardCreateServlet")
public class CoachBoardCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public CoachBoardCreateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		Coach coachInfo = (Coach)session.getAttribute("coachInfo");
		if(coachInfo == null) {
			response.sendRedirect("IndexServlet");
			return;
		}
		session.setAttribute("coachInfo", coachInfo);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coachboardcreate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession(false);
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		Coach coachInfo = (Coach)session.getAttribute("coachInfo");
		int coach_id = coachInfo.getId();

		if(title.equals("") || content.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coachboardcreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		CoachBoardDao boarddao = new CoachBoardDao();
		boarddao.CreateCoachBoard(title,content,coach_id);
		response.sendRedirect("CoachTopServlet");
	}

}
