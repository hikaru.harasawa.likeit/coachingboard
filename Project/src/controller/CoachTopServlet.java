package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserBoardDao;
import model.Board;
import model.Coach;


@WebServlet("/CoachTopServlet")
public class CoachTopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public CoachTopServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		Coach coachInfo = (Coach)session.getAttribute("coachInfo");
		if(coachInfo == null) {
			response.sendRedirect("IndexServlet");
			return;
		}

		UserBoardDao boarddao = new UserBoardDao();
		List<Board> boardList = boarddao.findLatest();
		request.setAttribute("boardList", boardList);
		session.setAttribute("coachInfo", coachInfo);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coachtop.jsp");
		dispatcher.forward(request, response);
	}

}
