package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BoardDao;
import model.Board;

@WebServlet("/BoardIndexServlet")
public class BoardIndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public BoardIndexServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		BoardDao boarddao = new BoardDao();
		List<Board> boardList = boarddao.findAll();
		request.setAttribute("boardList", boardList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/boardindex.jsp");
		dispatcher.forward(request, response);
	}
}
