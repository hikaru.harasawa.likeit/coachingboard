package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BoardDao;
import model.Board;
import model.Coach;

@WebServlet("/BoardDeleteCheckServlet")
public class BoardDeleteCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public BoardDeleteCheckServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		Coach coachInfo = (Coach)session.getAttribute("coachInfo");
		if(coachInfo == null) {
			response.sendRedirect("IndexServlet");
			return;
		}

		int board_id = Integer.parseInt(request.getParameter("id"));
		BoardDao boarddao = new BoardDao();
		Board board = boarddao.findBoardByBoardId(board_id);
		request.setAttribute("board", board);
		session.setAttribute("coachInfo",coachInfo);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/boarddelete.jsp");
		dispatcher.forward(request, response);
	}


}
