package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CoachDao;
import model.Coach;


@WebServlet("/CoachIndexServlet")
public class CoachIndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public CoachIndexServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		CoachDao coachdao = new CoachDao();
		List<Coach> coachList = coachdao.findAll();
		request.setAttribute("coachList", coachList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coachindex.jsp");
		dispatcher.forward(request, response);
	}
}
