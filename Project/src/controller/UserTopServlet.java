package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CoachBoardDao;
import dao.CoachDao;
import model.Board;
import model.Coach;
import model.User;

@WebServlet("/UserTopServlet")
public class UserTopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserTopServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		User userInfo = (User)session.getAttribute("userInfo");
		if(userInfo == null) {
			response.sendRedirect("IndexServlet");
			return;
		}

		CoachBoardDao boarddao = new CoachBoardDao();
		List<Board> boardList = boarddao.findLatest();
		CoachDao coachdao = new CoachDao();
		List<Coach> coachList = coachdao.findLatest();
		request.setAttribute("boardList", boardList);
		request.setAttribute("coachList", coachList);
		session.setAttribute("userInfo", userInfo);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/usertop.jsp");
		dispatcher.forward(request, response);
	}

}
