package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserBoardDao;
import model.Board;
import model.User;

@WebServlet("/UserBoardDeleteCheckServlet")
public class UserBoardDeleteCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserBoardDeleteCheckServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		User userInfo = (User)session.getAttribute("userInfo");
		if(userInfo == null) {
			response.sendRedirect("IndexServlet");
			return;
		}

		int board_id = Integer.parseInt(request.getParameter("id"));
		UserBoardDao boarddao = new UserBoardDao();
		Board board = boarddao.findUserBoardByBoardId(board_id);
		request.setAttribute("board", board);
		session.setAttribute("userInfo", userInfo);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userboarddelete.jsp");
		dispatcher.forward(request, response);
	}

}
