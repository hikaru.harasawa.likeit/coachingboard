package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import dao.CoachDao;
import model.Coach;


@WebServlet("/CoachUpdateServlet")
@MultipartConfig(location="/Applications/Eclipse_4.7.3.app/Contents/workspace/CoachingBoard/WebContent/image", maxFileSize=1048576)
public class CoachUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public CoachUpdateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		Coach coachInfo = (Coach)session.getAttribute("coachInfo");
		if(coachInfo == null) {
			response.sendRedirect("IndexServlet");
			return;
		}

		int coach_id = Integer.parseInt(request.getParameter("id"));
		CoachDao coachdao = new CoachDao();
		Coach coach = coachdao.findCoachByCoachId(coach_id);
		request.setAttribute("coach", coach);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coachupdate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");
		int age = Integer.parseInt(request.getParameter("age"));
		String introduction = request.getParameter("introduction");

		Part imageFile = request.getPart("image");
		String image = getFileName(imageFile);
		imageFile.write(image);
		int coach_id = Integer.parseInt(request.getParameter("id"));

		if(name.equals("") || age == 0 || image.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coachupdate.jsp");
			dispatcher.forward(request, response);
		}

		CoachDao coachdao = new CoachDao();
		coachdao.UpdateCoach(name,age,introduction,image,coach_id);
		response.sendRedirect("IndexServlet");
	}

	private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }

}
