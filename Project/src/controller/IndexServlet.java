package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BoardDao;
import dao.CoachDao;
import model.Board;
import model.Coach;
import model.User;


@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public IndexServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User userInfo = (User)session.getAttribute("userInfo");
		Coach coachInfo = (Coach)session.getAttribute("coachInfo");

		BoardDao boarddao = new BoardDao();
		List<Board> boardList = boarddao.findLatest();
		CoachDao coachdao = new CoachDao();
		List<Coach> coachList = coachdao.findLatest();
		request.setAttribute("boardList", boardList);
		request.setAttribute("coachList", coachList);
		session.setAttribute("userInfo", userInfo);
		session.setAttribute("coachInfo", coachInfo);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
		dispatcher.forward(request, response);
	}
}
