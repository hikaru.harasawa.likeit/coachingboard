package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserBoardDao;

@WebServlet("/UserBoardDeleteServlet")
public class UserBoardDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserBoardDeleteServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		int board_id = Integer.parseInt(request.getParameter("id"));
		UserBoardDao boarddao = new UserBoardDao();
		boarddao.DeleteUserBoard(board_id);
		response.sendRedirect("UserTopServlet");
	}

}
