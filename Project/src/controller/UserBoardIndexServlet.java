package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserBoardDao;
import model.Board;

@WebServlet("/UserBoardIndexServlet")
public class UserBoardIndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserBoardIndexServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		UserBoardDao boarddao = new UserBoardDao();
		List<Board> boardList = boarddao.findAll();
		request.setAttribute("boardList", boardList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userboardindex.jsp");
		dispatcher.forward(request, response);
	}

}
